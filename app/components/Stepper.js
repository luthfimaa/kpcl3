import React, { Component } from 'react';
import { 
    Alert,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    StyleSheet,
} from 'react-native';

import OrderController from '../controllers/OrderController';
import CartController from '../controllers/CartController';

export default class Stepper extends Component{
    constructor(props){
        super(props);
        this.state = {
            order: props.order,
            quantity: props.order.quantity.toString(),
            user: props.user,
        };
    }

    decrement(){
        let q = parseInt(this.state.quantity)-1;
        if(q != 0){
            this.setState({quantity: q.toString()});
            OrderController.changeQuantity(this.state.order, q);
        }
        else{
            this.props.remove(this.state.order);
            CartController.removeOrder(this.state.user.cartId, this.state.order);
        }
        this.props.getPrice();
    }

    increment(){
        let q = parseInt(this.state.quantity)+1;
        this.setState({quantity: q.toString()});
        OrderController.changeQuantity(this.state.order, q);
        this.props.getPrice();
    }

    render(){
        return(
            <View
            style={styles.container}
            >
                <TouchableOpacity 
                onPress={this.decrement.bind(this)}
                style={styles.item}>
                    <Text style={styles.text}>
                        -
                    </Text>
                </TouchableOpacity>
                <TextInput 
                underlineColorAndroid='transparent'
                style={styles.field}
                value={this.state.quantity}
                />
                <TouchableOpacity 
                onPress={this.increment.bind(this)}
                style={styles.item}>
                    <Text style={styles.text}>
                        +
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

let primary = '#F6362F';
let fieldBg = 'white';
let color = primary;
let itemSize = 35;
let styles = StyleSheet.create({
    item: {
        padding: 5,
        backgroundColor: 'white',
        width: itemSize,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: primary,
    },
    field: {
        padding: 8,
        width: itemSize,
        backgroundColor: fieldBg,
    },

    container: {
        flex: 1,
        flexDirection: 'row',
        width: itemSize * 3,
        maxHeight: 50,
        backgroundColor: primary,
        borderWidth: 1,
        borderRadius: 7,
        overflow: 'hidden',
        borderColor: color,
    },
    text: {
        fontSize: 30,
        color: color,
    }
});
