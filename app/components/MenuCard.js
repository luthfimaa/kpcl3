import React, { Component } from 'react';
import {
    Container, 
    Header, 
    Button, 
    Body,
    Content, 
    Card,
    CardItem,
    Drawer,
    List,
    ListItem,
    Left,
    Fab,
    Footer,
    FooterTab,
    Grid,
    Row,
    Col,
    Form, 
    Item, 
    Icon,
    Input, 
    Label, 
    Text, 
    Tab, 
    Tabs, 
    TabHeading, 
    Toast,
    Right,
} from 'native-base';
import {Alert} from 'react-native';
import MenuController from '../controllers/MenuController';
import CartController from '../controllers/CartController';
import {styles} from '../styles/styles';

export default class MenuCard extends Component{
    constructor(props){
        super(props);
        this.state = {
            user: props.user,
            menu: props.item,
            inCart: props.inCart,
            clicked: false,
            navigate: props.navigate,
        };
    }

    order(menu){
        CartController.addToCart(this.state.user, menu);
        this.setState({
            clicked: true,
        });
    }

    onPressDelete(item){
        MenuController.deleteMenu(item)
            .then(() => {
                Alert.alert('', 'Menu Berhasil Dihapus', [{ text: 'Lanjutkan', onPress: () => this.state.navigate('StandList', {user: this.state.user})}
                ]);

            })
    }
    render(){
        return(
            <ListItem
            >
                {(this.state.user.role == 'guest') &&
                <Grid
                    style={{
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                    }}
                    >
                    <Col size={4}
                    style={{
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                    }}
                    >
                            <Text>
                                {this.state.menu.name}
                            </Text>
                            <Text>
                                Rp. {this.state.menu.price}
                            </Text>
                    </Col>
                    <Col size={3}>
                    </Col>
                </Grid>
                }
                {(this.state.user.role == 'member') &&
                <Grid
                    style={{
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                    }}
                    >
                <Col size={4}
                style={{
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                }}
                >
                        <Text>
                            {this.state.menu.name}
                        </Text>
                        <Text>
                            Rp. {this.state.menu.price}
                        </Text>
                </Col>
                <Col size={3}>
                </Col>

                <Col size={4}
                >
                    <Row >
                        <Button iconRight rounded bordered
                        disabled={this.state.inCart || this.state.clicked}
                        onPress={this.order.bind(this, this.state.menu)}>
                            <Text>Tambah</Text>
                            <Icon name='ios-cart'/>
                        </Button>
                    </Row>
                </Col>
                </Grid>
                }
                {this.state.user.role == 'admin' &&
                <Grid style={{flex: 1}}>
                    <Col size={6}>
                        <Row>
                        </Row>
                        <Row>
                            <Text>
                                {this.state.menu.name}
                            </Text>
                        </Row>
                        <Row>
                            <Text>
                                {this.state.menu.price}
                            </Text>
                        </Row>
                        <Row>
                        </Row>
                    </Col>
                    <Col size={4}>
                        <Row
                        style={{marginBottom: 10, flex: 1}}
                        >
                            <Button blocked rounded bordered dark iconRight
                            onPress={() => {this.state.navigate('MenuForm', {
                                mode: 'edit',
                                menu: this.state.menu,
                            })}}>
                                <Text>
                                    Edit
                                </Text>
                                <Icon name='ios-settings'/>

                            </Button>
                        </Row>
                        <Row>
                            <Button rounded bordered
                            onPress={this.onPressDelete.bind(this, this.state.menu)}
                            >
                                <Text>
                                    Hapus
                                </Text>
                                <Icon name='ios-close'/>
                            </Button>
                        </Row>
                    </Col>
                </Grid>
                }
            </ListItem>
        );
    }
}
