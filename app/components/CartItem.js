
import React, { Component } from 'react';
import {
    Container, 
    Header, 
    Button, 
    Body,
    Content, 
    Card,
    CardItem,
    Col,
    Form,
    Footer,
    FooterTab,
    Grid,
    Input,
    Item,
    Label,
    List,
    ListItem,
    Right,
    Text, 
} from 'native-base';
import {
    Alert,
    StyleSheet,
    RefreshControl
} from 'react-native';

import {styles} from '../styles/styles';

import StandController from '../controllers/StandController';
import CartController from '../controllers/CartController';
import Stepper from '../components/Stepper';

export default class CartItem extends Component{
    constructor(props){
        super(props);
        this.state = {
            item: props.item,
            user: props.user,
        };
    }
    render(){
        return(
            <ListItem>
                <Grid>
                <Col size={3}>
                    <Text>
                        {this.state.item.name}
                    </Text>
                    <Text>
                        Rp. {this.state.item.price}
                    </Text>
                </Col>
                <Col size={3}>
                </Col>
                <Col size={4}>
                    <Stepper
                    user={this.state.user}
                    order={this.state.item}
                    getPrice={this.props.getPrice.bind(this)}
                    remove={this.props.remove}
                    />
                </Col>
                </Grid>
            </ListItem>
        );
    }
}
