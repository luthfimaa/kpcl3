import React, { Component } from 'react';
import {
    Container, 
    Header, 
    Button, 
    Body,
    Content, 
    Card,
    CardItem,
    Col,
    Form,
    Footer,
    FooterTab,
    Grid,
    Input,
    Item,
    Icon,
    Label,
    List,
    ListItem,
    Right,
    Row,
    Text, 
} from 'native-base';
import {colors} from '../styles/styles';
import {Alert, StyleSheet } from 'react-native';
import AuthController from '../controllers/AuthController';

export default class SideBar extends Component{
    render(){
        return(
            <Container
            style={styles.container}>
                <Content style={styles.container}>
                    <Grid>
                        <Row style={[styles.row, {paddingTop: 20}]}>
                            <Text style={{
                                fontSize: 24,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                                {this.props.user.name}
                            </Text>
                        </Row>
                        <Row style={styles.row}>
                            <Text>
                                {this.props.user.username}
                            </Text>
                        </Row>
                        {this.props.user.role == 'member' &&
                        <Row style={styles.row}>
                            <Text>
                                Saldo : Rp {this.props.user.balance}
                            </Text>
                        </Row>
                        }
                    </Grid>
                </Content>
                <Footer
                >
                    {(this.props.user.role != 'guest') &&
                    <FooterTab
                    style={{backgroundColor: 'white'}}
                    
                    >
                        <Button transparent primary
                        onPress={() => {
                            AuthController.logout(this.props.user);
                            this.props.navigate('Welcome');
                        }}
                        >
                            <Text>
                                Logout
                            </Text>
                        </Button>
                    </FooterTab>
                    }
                    {(this.props.user.role == 'guest') &&
                    <FooterTab
                    style={{backgroundColor: 'white'}}
                    
                    >
                        <Button transparent primary
                        onPress={() => {
                            this.props.navigate('Welcome');
                        }}
                        >
                            <Text>
                                Login
                            </Text>
                        </Button>
                    </FooterTab>
                    }
                </Footer>
            </Container>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'white',
    },
    row: {
        justifyContent: 'center',
        padding: 3,
    }
});
