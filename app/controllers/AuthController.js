import User from '../models/User';
import Cart from '../models/Cart';
import {
    Alert,
    AsyncStorage,
} from 'react-native';

export default class AuthController{
    static register(email, password,  username, name, phone){
        return User.create(email, password)
            .then(uid => {
                let cartId = Cart.create(uid)
                User.writeUserData(uid, email, username, name, phone, cartId);
            });
    }

    static login(email, password){
        return User.login(email, password)
            .then(uid => {
                return User.get(uid)
                    .then(user => {
                        try{
                            AsyncStorage.setItem('@User:key', JSON.stringify(user.toJSON()));
                        } catch(error) {
                            Alert.alert(error);
                        }
                        return user.toJSON();
                    });
            })
            .catch(error => console.log(error));
    }

    static logout(userJSON){
        let user = User.fromJSON(userJSON);
        user.logout();
        AsyncStorage.clear();
    }
}
