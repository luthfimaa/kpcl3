import Menu from '../models/Menu';
import {Alert} from 'react-native';

export default class MenuController{
    static getMenusBelongToStand(standId){
        return Menu.getMenusBelongToStand(standId)
            .then(menuList => {
                let menuListJSON = [];
                menuList.forEach(menu => {
                    menuListJSON.push(menu.toJSON());
                });
                return menuListJSON;
            });
    }

    static createMenu(name, price, standId){
        return Menu.create(name, price, standId);
    }

    static editMenu(menuId, name, price, standId){
        return Menu.get(menuId)
            .then(menu => {
                menu.edit(name, price, standId);
            })

    }

    static deleteMenu(menuJSON){
        let menu = Menu.fromJSON(menuJSON);
        return menu.delete();
    }
}
