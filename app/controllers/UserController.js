import User from '../models/User';

export default class UserController{
    static topupBalance(username, amount){
        return User.getByUsername(username)
            .then(user => {
                let currentBalance = user.balance;
                user.setBalance(currentBalance+amount);

            })
    }

    static getCurrentUser(){
        return User.getCurrentUser()
            then(user => {return user.toJSON()});
    }
}
