import User from '../models/User';
import Order from '../models/Order';
import Stand from '../models/Stand';
import {Alert} from 'react-native';

export default class OrderController{
    static getStandOrder(standId){
        return Order.getOrdersBelongToStand(standId)
            .then(orders => {
                let ordersJSON = [];
                orders.forEach(order => ordersJSON.push(order.toJSON()));
                return ordersJSON;
            })
    }

    static acceptOrder(orderJSON){
        let order = Order.fromJSON(orderJSON);
        order.accept();
    }

    static refuseOrder(orderJSON){
        let order = Order.fromJSON(orderJSON);
        order.refuse();
    }

    static changeQuantity(orderJSON, quantity){
        let order = Order.fromJSON(orderJSON);
        order.changeQuantity(quantity);
    }

    static getTotalSales(standId){
        return OrderController.getStandOrder(standId)
            .then(orderList => {
                let total = 0;
                orderList.forEach(order => {
                    total += order.quantity * order.price;
                });
                return total;
            });
    }

}
