import Cart from '../models/Cart';
import Menu from '../models/Menu';
import Order from '../models/Order';
import User from '../models/User';

export default class CartController{
    static addToCart(userJSON, menuJSON){
        let menu = Menu.fromJSON(menuJSON);
        let user = User.fromJSON(userJSON);
        Cart.get(user.cartId)
            .then(cart => {
                let order = Order.create(user.id, menu);
                cart.add(order);

            })
            .catch(error => console.log(error));
    }

    static getOrderList(cartId){
        return Cart.get(cartId)
            .then(cart => {
                let orderList = [];
                for(var orderId in cart.items){
                    if(orderId != 'dummy') orderList.push(Order.get(orderId));
                }
                return Promise.all(orderList);
            })
            .then(orderList => {
                let orderListJSON = [];
                orderList.forEach(order => orderListJSON.push(order.toJSON()));
                return orderListJSON;
            })
            .catch(error => console.log(error.toString()));
    }

    static getTotalPrice(cartId){
        let totalPrice = 0;
        return CartController.getOrderList(cartId)
            .then(orderList => {
                orderList.forEach(order => totalPrice += order.quantity * order.price);
                return totalPrice;
            })
            .catch(error => console.log(error));
    }

    static setTableNumber(cartId, tableNumber){
        Cart.get(cartId)
            .then(cart => {
                let orderList = [];
                for(var orderId in cart.items){
                    orderList.push(Order.get(orderId));
                }
                return Promise.all(orderList);
            })
            .then(orderList => {
                orderList.forEach(order => order.setTableNumber(tableNumber));
            })
    }

    static purchase(userJSON){
        let user = User.fromJSON(userJSON);
        CartController.getTotalPrice(user.cartId).then(totalPrice => {
            user.setBalance(user.balance - totalPrice);
            try{
                AsyncStorage.setItem('@User:key', JSON.stringify(user.toJSON()));
            } catch(error) {
                Alert.alert(error);
            }
        })
        return Cart.get(user.cartId)
            .then(cart => {
                let orderList = [];
                for(var orderId in cart.items){
                    orderList.push(Order.get(orderId));
                }
                cart.clear();
                return Promise.all(orderList);
            })
            .then(orderList => {
                orderList.forEach(order => order.order());
            })
    }

    static removeOrder(cartId, orderJSON){
        let order = Order.fromJSON(orderJSON);
        Cart.get(cartId)
            .then(cart => cart.remove(order.id))
            .then(() => order.delete())
            .catch(error => console.log(error));
    }
}
