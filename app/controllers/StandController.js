import Stand from '../models/Stand';

export default class StandController{
    static getUserStand(userId){
        return Stand.getUserStand(userId);
    }
    static createStand(displayName, userId){
        Stand.create(displayName, userId);
    }

    static getStandList(){
        return Stand.getStandList()
            .then(standList => {
                standListJSON = [];
                standList.forEach(stand => standListJSON.push(stand.toJSON()));
                return standListJSON;
            })
    }
}
