import {firebaseApp} from '../config/firebase';

export default class User{
    static create(email, password){
        return firebaseApp.auth().createUserWithEmailAndPassword(email, password)
            .then(user => {return user.uid;});
    }

    static writeUserData(id, email,  username, name, phone, cartId){
        firebaseApp.database().ref('users/' + id).set({
            email: email,
            name: name,
            username: username,
            balance: 0,
            role: 'member',
            phone: phone,
            cartId: cartId,
        });
    }

    toJSON(){
        return {
            id       : this.id,
            cartId   : this.cartId,
            email    : this.email,
            name     : this.name,
            username : this.username,
            phone    : this.phone,
            balance  : this.balance,
            role     : this.role,
        }
    }

    _update(){
        let userJSON = this.toJSON();
        delete userJSON.id;
        firebaseApp.database().ref('users/' + this.id).update(userJSON);
    }

    static fromJSON(userJSON){
        let user = new User();
        user.id = userJSON.id;
        user.balance = userJSON.balance;
        user.cartId = userJSON.cartId;
        user.email = userJSON.email;
        user.name = userJSON.name;
        user.username = userJSON.username;
        user.phone = userJSON.phone;
        user.role = userJSON.role;
        return user;
    }

    static get(id){
        let userRef = firebaseApp.database().ref('users/' + id);
        return userRef.once('value').then(snap => {
            let userJSON = snap.val();
            userJSON['id'] = id;
            return User.fromJSON(userJSON);
        });
    }

    static getByUsername(username){
        let userRef = firebaseApp.database().ref('users/');
        return userRef.orderByChild('username').equalTo(username).limitToLast(1).once('value')
            .then(snap => {
                let userJSON = {};
                for(var id in snap.val()){
                    userJSON = snap.val()[id];
                    userJSON.id = id;
                }
                return User.fromJSON(userJSON);
            })

    }

    static login(email, password){
        return firebaseApp.auth().signInWithEmailAndPassword(email, password)
            .then(user => {return user.uid;})
            .catch(error => console.log(error));
    }

    static getCurrentUser(){
        return new Promise(function(resolve){
            resolve(firebaseApp.auth().currentUser.uid);
        }).then((id) => {
            return User.get(id);
        });
    }

    setBalance(amount){
        this.balance = amount;
        this._update();
    }

    logout(){
        firebaseApp.auth().signOut();
    }
}
