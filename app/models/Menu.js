import {firebaseApp} from '../config/firebase';
import {Alert} from 'react-native';

export default class Menu{
    /*
       DB Structure:
       menus: {
        menuId:
            name:
            price:
            standId:
       }
    */
    static create(name, price, standId){
        let menuRef = firebaseApp.database().ref('menus').push();
        let menu     = new Menu();
        menu.id      = menuRef.key;
        menu.name    = name;
        menu.price   = price;
        menu.standId = standId;

        let menuJSON = menu.toJSON();
        delete menuJSON.id;
        menuRef.set(menuJSON);
        return menu;
    }

    _update(){
        const menuRef = firebaseApp.database().ref('menus/' + this.id);
        menuRef.update(this.toJSON());
    }

    delete(){
        const menuRef = firebaseApp.database().ref('menus/' + this.id);
        return menuRef.remove();
    }

    toJSON(){
        return{
            id: this.id,
            name: this.name,
            price: this.price,
            standId: this.standId,
        };
    }

    edit(name, price, standId){
        this.name = name;
        this.price = price;
        this.standId = standId;
        this._update();
    }

    static fromJSON(menuJSON){
        let menu = new Menu();
        menu.id      = menuJSON.id;
        menu.name    = menuJSON.name;
        menu.price   = menuJSON.price;
        menu.standId = menuJSON.standId;
        return menu;
    }

    static fromOrderJSON(menuJSON){
        let menu = new Menu();
        menu.id      = menuJSON.menuId;
        menu.name    = menuJSON.name;
        menu.price   = menuJSON.price;
        menu.standId = menuJSON.standId;
        return menu;
    }

    static get(id){
        const menusRef = firebaseApp.database().ref('menus/' + id);
        return menusRef.once('value').then(function(snap){
            let menu = snap.val();
            menu.id = snap.key;
            return Menu.fromJSON(menu);
        }).catch((error) => {
            console.log('[DEBUG] failed to get menu');
            console.log(error);
        });
    }

    
    static getMenusBelongToStand(standId){
        const menusRef = firebaseApp.database().ref('menus');
        const menusByStandRef = menusRef.orderByChild('standId').equalTo(standId);
        return menusByStandRef.once('value').then(function(menus){
            menuList = [];
            menus.forEach((snap) => {
                let menu = snap.val();
                menu.id = snap.key;
                menuList.push(Menu.fromJSON(menu));
            });
            return menuList;
        }).catch((error) => {
            console.log('[DEBUG] failed to get menu belong to stand');
            console.log(error);
        });
    }
}
