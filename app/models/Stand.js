import {firebaseApp} from '../config/firebase';
import {Alert} from 'react-native';

export default class Stand{
    /*
       stands/
        standId
            displayName:
            userId:
    */

    static create(displayName, userId){
        const newKey = firebaseApp.database().ref('stands').push().key;
        const standRef = firebaseApp.database().ref('stands/' + newKey);
        standRef.set({
            displayName: displayName,
            userId: userId,
        });
    }
    static getStandList(){
        const standRef = firebaseApp.database().ref('stands/');
        return standRef.once('value').then(stands => {
            var standList = [];
            stands.forEach(stand => {
                let standJSON = stand.val();
                standJSON.id = stand.key;
                standList.push(Stand.fromJSON(standJSON));
            });
            return standList;
        });
    }

    static getUserStand(userId){
        const standRef = firebaseApp.database().ref('stands/').orderByChild('userId').equalTo(userId).limitToLast(1);
        return standRef.once('value').then(stand => {
            let id = Object.keys(stand.val())[0];
            return Stand.get(id);
        });
    }

    static get(id){
        const standRef = firebaseApp.database().ref('stands/' + id);
        return standRef.once('value').then(snap => {
            let standJSON = snap.val();
            standJSON.id = snap.key;
            return Stand.fromJSON(standJSON); 
        });

    }

    static fromJSON(standJSON){
        let stand         = new Stand();
        stand.id          = standJSON.id;
        stand.displayName = standJSON.displayName;
        stand.userId      = standJSON.userId;
        return stand;
    }

    toJSON(){
        return {
            id: this.id,
            displayName: this.displayName,
            userId: this.userId
        };
    }
}
