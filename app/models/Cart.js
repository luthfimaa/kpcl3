import {firebaseApp} from '../config/firebase';
import * as firebase from 'firebase';
import {Alert} from 'react-native';

export default class Cart{
    /* structure:
      cartId/
        items : {
          orderId: true,
          orderId: true,
        }
        updatedAt
    */

    _update(){
        const cartRef = firebaseApp.database().ref('carts/' + this.id);
        this.updatedAt = firebase.database.ServerValue.TIMESTAMP;
        cartRef.update(this.toJSON());
    }

    toJSON(){
        return {
            items: this.items,
            updatedAt: this.updatedAt,
        };
    }

    static create(userId){
        const newKey = firebaseApp.database().ref('carts').push().key;
        const cartRef = firebaseApp.database().ref('carts/' + newKey);
        cartRef.set({
            items     : {dummy: true},
            updatedAt : firebase.database.ServerValue.TIMESTAMP,
        });
        return newKey;
    }

    static get(id){
        const carts = firebaseApp.database().ref('carts/' + id);
        return carts.once('value').then((snap) => {
            let cartJSON = snap.val();
            cartJSON['id'] = id;
            return Cart.fromJSON(cartJSON);
        }).catch(error => console.log(error));
    }

    static fromJSON(cartJSON){
        let cart = new Cart();
        cart.id = cartJSON.id;
        cart.items = cartJSON.items;
        return cart;
    }

    add(order){
        if(this.items.dummy != undefined){
            delete this.items.dummy;
        }
        this.items[order.id] = true;
        this._update();
    }

    remove(orderId){
        let itemCount = Object.keys(this.items).length;
        if(itemCount == 1){
            this.items['dummy'] = true;
        }
        delete this.items[orderId];
        this._update();
    }

    clear(){
        this.items = {dummy: true};
        this._update();
    }

}
