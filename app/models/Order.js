import {firebaseApp} from '../config/firebase';
import * as firebase from 'firebase';
import Menu from './Menu';

export default class Order{
    /*
       DB Structure:
       orders: {
            orderId: {
                menuId : 
                name:
                price:
                quantity:
                standId:
                status:
                tableNumber:
                updatedAt:
                userId:
            }
       }

    */
    _update(){
        const orderRef = firebaseApp.database().ref('orders/' + this.id);
        this.updatedAt = firebase.database.ServerValue.TIMESTAMP,
        orderJSON = this.toJSON();
        delete orderJSON.id;
        orderRef.update(orderJSON);
    }

    static create(userId, menu){
        const orderRef    = firebaseApp.database().ref('orders').push();
        let order         = new Order();
        order.id          = orderRef.key;
        order.menu        = menu;
        order.quantity    = 1;
        order.status      = 'in-cart';
        order.tableNumber = 0;
        order.updatedAt   = firebase.database.ServerValue.TIMESTAMP;
        order.userId      = userId;

        let orderJSON     = order.toJSON();
        delete orderJSON.id;
        orderRef.set(orderJSON)
            .catch(error => console.log(error));
        return order;
    }

    static get(id){
        const orderRef = firebaseApp.database().ref('orders/' + id);
        return orderRef.once('value')
            .then(function(snap){
                let orderJSON = snap.val();
                orderJSON['id'] = id;
                return Order.fromJSON(orderJSON);
            })
            .catch(error => console.log(error));
    }

    static getOrdersBelongToStand(standId){
        const ordersRef = firebaseApp.database().ref('orders');
        const standOrders = ordersRef.orderByChild('standId').equalTo(standId);
        return standOrders.once('value').then(function(orders){
            var standOrders = [];
            orders.forEach(function(snap){
                const orderJSON = snap.val();
                orderJSON.id = snap.key;
                standOrders.push(Order.fromJSON(orderJSON));
            });
            return standOrders;
        });
    }

    //static getOrdersBelongToUser(userId){
    //    const ordersRef = firebaseApp.database().ref('orders');
    //    const standOrders = ordersRef.orderByChild('userId').equalTo(userId);
    //    return standOrders.once('value').then(function(orders){
    //        var userOrders = [];
    //        orders.forEach(function(snap){
    //            const orderJSON = snap.val();
    //            orderJSON.id = snap.key;
    //            standOrders.push(Order.fromJSON(orderJSON));
    //        });
    //        return standOrders;
    //    });
    //}

    static fromJSON(orderJSON){
        let order         = new Order();
        order.id          = orderJSON.id;
        order.quantity    = orderJSON.quantity;
        order.status      = orderJSON.status;
        order.tableNumber = orderJSON.tableNumber;
        order.userId      = orderJSON.userId;
        order.updatedAt   = orderJSON.updatedAt;
        order.menu        = Menu.fromOrderJSON(orderJSON);
        return order;
    }


    setTableNumber(tableNumber){
        this.tableNumber = tableNumber;
        this._update();
    }

    accept(){
        this.status = 'accepted';
        this._update();
    }

    refuse(){
        this.status = 'refused';
        this._update();
    }

    delete(){
        const orderRef = firebaseApp.database().ref('orders/' + this.id);
        orderRef.remove();
    }

    order(){
        this.status = 'pending';
        this._update();
    }

    toJSON(){
        let orderJSON = {
            id          : this.id,
            menuId      : this.menu.id,
            name        : this.menu.name,
            price       : this.menu.price,
            quantity    : this.quantity,
            standId     : this.menu.standId,
            status      : this.status,
            tableNumber : this.tableNumber,
            userId      : this.userId,
            updatedAt   : this.updatedAt,
        };
        return orderJSON;
    }

    changeQuantity(quantity){
        this.quantity = quantity;
        this._update();
    }
}
