import * as firebase from 'firebase';
import storage from 'firebase/storage';

const config = {
    apiKey: "AIzaSyAuwDCAzANyIDP-wuiWbm8iq7ft_5-eksQ",
    authDomain: "kpcl-3cb51.firebaseapp.com",
    databaseURL: "https://kpcl-3cb51.firebaseio.com",
    projectId: "kpcl-3cb51",
    storageBucket: "kpcl-3cb51.appspot.com",
    messagingSenderId: "681322058460"
};

firebaseApp = firebase.initializeApp(config);
storageRef = firebaseApp.storage().ref();
export { 
    firebaseApp,
    storageRef,
};
