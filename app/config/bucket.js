import Storage from '@google-cloud/storage';

let serviceAccountKey = "../data/kpcl-3cb51-firebase-adminsdk-d8zv1-1d03454d9c.json";
let projectId = "kpcl-3cb51";
let bucketName = `${projectId}.appspot.com`;

let storage = Storage({
    projectId: projectId,
    keyFilename: serviceAccountKey,
});

let bucket = storage.bucket(bucketName);
export { bucket };
