import React, { Component } from 'react';
import {
    Container, 
    Header, 
    Button, 
    Body,
    Content, 
    Card,
    CardItem,
    Drawer,
    List,
    ListItem,
    Left,
    Fab,
    Footer,
    FooterTab,
    Grid,
    Col,
    Form, 
    Item, 
    Icon,
    Input, 
    Label, 
    StyleProvider,
    Text, 
    Tab, 
    Tabs, 
    TabHeading, 
    Title,
    Right,
} from 'native-base';

import OrderController from '../controllers/OrderController';
import SideBar from '../components/SideBar';
import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

import { 
    StyleSheet,
    Alert,
} from 'react-native';

import {
    Table, 
    Row,
    Rows
} from 'react-native-table-component'
export default class Transaction extends Component{
    constructor(props){
        super(props);
        this.state = {
            stand: this.props.navigation.state.params.stand,
            tableHead: ['Nama Makanan', 'Jumlah', 'Status', 'Total'],
            tableData: [],
            total: '0',
        };
    }

    componentWillMount(){
        OrderController.getStandOrder(this.state.stand.id)
            .then(orderList => {
                let data = [];
                orderList.forEach(order => {
                    total = order.price * order.quantity;
                    let row = [order.name, order.quantity, order.status, total];
                    data.push(row);
                });
                this.setState({
                    tableData: data,
                });
            });

        OrderController.getTotalSales(this.state.stand.id)
            .then(total => this.setState({total: total.toString()}));
    }

    render(){
        return(
    <StyleProvider style={getTheme(commonColor)}>
        <Container>
                <Header 
                androidStatusBarColor={variables.statusBar}
                style={{backgroundColor:'white'}}>
                    <Body style={{flex: 1, justifyContent: 'center', aligntItems: 'center'}}>
                        <Title
                        style={{color: 'black'}}
                        >
                            Transaksi Penjual
                        </Title>
                    </Body>
                </Header>
            <Content style={styles.content}>
                <Table>
                  <Row data={this.state.tableHead} style={styles.head} textStyle={styles.text}/>
                  <Rows data={this.state.tableData} style={styles.row} textStyle={styles.text}/>
                  <Row data={['Total Penjualan', this.state.total]}/>
                </Table>
            </Content>
        </Container>        
    </StyleProvider>
        );
    }
}
