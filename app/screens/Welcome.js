import React, {Component} from 'react';
import { 
    Button, 
    Container,
    Content,
    Header,
    StyleProvider,
    Text, 
} from 'native-base';
import {
    Col,
    Grid,
    Row,
} from 'react-native-easy-grid';

import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

export default class Welcome extends Component{
    render(){
        return(
            <StyleProvider style={getTheme(commonColor)}>
                <Container>
                    <Header androidStatusBarColor={variables.statusBar} style={{display:'none'}}/>
                    <Grid style={{flex: 1, backgroundColor: 'white'}}>
                        <Row size={80}>
                            <Text>
                            </Text>
                        </Row>
                        <Row size={10}>
                            <Col 
                            size={1}
                            style={styles.padButton}
                            >
                                <Button block rounded
                                onPress={() => {this.props.navigation.navigate("Register")}}
                                >
                                    <Text>Register</Text>
                                </Button>
                            </Col>
                        </Row>
                        <Row size={10}>
                            <Col 
                            size={1}
                            style={styles.padButton}
                            >
                                <Button block rounded
                                onPress={() => {this.props.navigation.navigate("Login")}}
                                >
                                    <Text>Login</Text>
                                </Button>

                            </Col>
                        </Row>
                    </Grid>
                </Container>
            </StyleProvider>
        );
    }
}
