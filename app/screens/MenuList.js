import React, { Component } from 'react';
import {
    Container, 
    Header, 
    Button, 
    Body,
    Content, 
    Card,
    CardItem,
    Drawer,
    List,
    ListItem,
    Left,
    Fab,
    Footer,
    FooterTab,
    Grid,
    Row,
    Col,
    Form, 
    Item, 
    Icon,
    Input, 
    Label, 
    Text, 
    Tab, 
    Tabs, 
    Title,
    StyleProvider,
    TabHeading, 
    Toast,
    Right,
} from 'native-base';
import MenuController from '../controllers/MenuController';
import CartController from '../controllers/CartController';
import SideBar from '../components/SideBar';
import MenuCard from '../components/MenuCard';

import { 
    Alert,
    Platform,
    RefreshControl,
    StyleSheet,
} from 'react-native';

import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

export default class MenuList extends Component{
    constructor(props){
        super(props);
        let user = props.navigation.state.params.user;
        this.state = {
            ds: [],
            user: user,
            navigate: props.navigation.navigate,
            stand: props.navigation.state.params.stand,
            cartItems: [],
            loading: false,
        };
    }

    getCartItems(){
        this.setState({loading: true});
        return CartController.getOrderList(this.state.user.cartId)
            .then(orderList => {
                this.setState({
                    cartItems: orderList,
                });
                this.setState({loading: false});
            })
            .catch(error => this.setState({loading: false}));
    }
    getItems(){
        MenuController.getMenusBelongToStand(this.state.stand.id)
            .then(menuList => {
                this.setState({ds: menuList});
            });
    }
    componentWillMount(){
        this.getCartItems()
            .then(() => this.getItems());
    }

    inCart(item){
        var found = false;
        for(var i in this.state.cartItems){
            var order = this.state.cartItems[i];
            if(item.id == order.menuId) return true;
        }
        return found;
    }

    order(item){
        CartController.addToCart(this.state.user, item);
    }

    renderRow(menu){
        return(
            <MenuCard
                item={menu}
                user={this.state.user}
                inCart={this.inCart(menu)}
                navigate={this.state.navigate}
            />
        );
    }

    openDrawer(){
        this._drawer._root.open();
    }

    render(){
        return(
    <StyleProvider style={getTheme(commonColor)}>
        <Drawer
        ref={ref => {this._drawer = ref}}
        content={
            <SideBar
                user={this.state.user}
                navigate={this.props.navigation.navigate}
            />
        }
        >
            <Container>
                <Header 
                androidStatusBarColor={variables.statusBar}
                style={{backgroundColor:'white'}}>
                <Left style={{flex: 1}}>
                    <Button transparent
                    onPress={this.openDrawer.bind(this)}
                    >
                        <Icon 
                        style={{color: variables.icon}}
                        name='menu'/>
                    </Button>
                </Left>
                <Body style={{flex: 1, justifyContent: 'center', aligntItems: 'center'}}>
                    <Title
                    style={{color: 'black'}}
                    >
                        Menu
                    </Title>
                </Body>
                {this.state.user.role == 'member' &&
                <Right>
                    <Button transparent
                    onPress={() => this.state.navigate('CartPage', {
                        user: this.state.user,
                    })}
                    >
                        <Icon 
                        style={{color: variables.icon}}
                        name='ios-cart'/>
                    </Button>
                </Right>
                }
            </Header>
            <Content
            refreshControl={
                <RefreshControl
                    refreshing={this.state.loading}
                />
            }
            contentContainerStyle={{flex: 1}}
            style={styles.content}
            >
                <List
                dataArray={this.state.ds}
                renderRow={this.renderRow.bind(this)}
                />
            </Content>
            { this.state.user.role == 'admin' &&
            <Footer>
                <FooterTab style={{backgroundColor: 'white'}}>
                    <Button transparent primary
                    onPress={() => {this.state.navigate('MenuForm', {
                        mode: 'create',
                        stand: this.state.stand,
                    })}}
                    >
                        <Text>
                            Buat Menu
                        </Text>
                    </Button>

                </FooterTab>
            </Footer>
            }
            </Container>
        </Drawer>
    </StyleProvider>
        );
    }
}
