import React, { Component } from 'react';
import {
    Container, 
    Header, 
    Button, 
    Body,
    Content, 
    Footer,
    FooterTab,
    Left,
    Picker,
    Form, 
    Item, 
    Input, 
    Icon,
    Label, 
    StyleProvider,
    Text, 
    Title,
} from 'native-base';

import { Alert, StyleSheet, NativeModule } from 'react-native';
import SideBar from '../components/SideBar';
import MenuController from '../controllers/MenuController';

const ImagePicker = require('react-native-image-picker');
const options = {
};
import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

export default class MenuForm extends Component{

    constructor(props){
        super(props);
        let mode = props.navigation.state.params.mode;
        if(mode == 'edit'){
            let menu = props.navigation.state.params.menu;
            this.state = {
                id: menu.id,
                name: menu.name,
                price: menu.price.toString(),
                standId: menu.standId,
                mode: mode,
            };
        }
        else{
            this.state = {
                name: '',
                price: '',
                stand: props.navigation.state.params.stand,
                mode: mode,
            };
        }
    }
    createMenu(){
        MenuController.createMenu(this.state.name, parseInt(this.state.price), this.state.stand.id);
        Alert.alert('Success');
        this.props.navigation.navigate('StandList');
    }

    editMenu(){
        MenuController.editMenu(this.state.id, this.state.name, this.state.price, this.state.standId)
        .then(() => Alert.alert('success'))
        .then(() => this.props.navigation.navigate('StandList'));
    }
    render(){
        return(
            <StyleProvider style={getTheme(commonColor)}>
            <Container>
                <Header 
                androidStatusBarColor={variables.statusBar} 
                style={{backgroundColor:'white'}}>
                <Body>
                    <Title style={{color: 'black'}}>
                        Form Menu
                    </Title>
                </Body>
                </Header>
                <Content
                style={styles.content}
                >
                <Form>
                    <Item>
                        <Icon name='ios-paper-outline'/>
                        <Input 
                        placeholder='Nama Menu'
                        value={this.state.name} onChangeText={text => this.setState({name: text})} />
                    </Item>
                    <Item>
                        <Icon name='logo-usd'/>
                        <Input 
                        placeholder='Harga'
                        value={this.state.price} 
                        onChangeText={text => this.setState({price: text})}
                        keyboardType="numeric"
                        />
                    </Item>
                </Form>
                </Content>
                <Footer>
                    <FooterTab style={{backgroundColor: 'white'}}>
                    {(this.state.mode == 'edit') &&
                        <Button transparent primary
                        onPress={this.editMenu.bind(this)}
                        block>
                            <Text>
                                Simpan
                            </Text>
                        </Button>
                    }
                    {(this.state.mode == 'create') &&
                        <Button transparent primary
                        onPress={this.createMenu.bind(this)}
                        block>
                            <Text>
                                Buat
                            </Text>
                        </Button>
                    }
                    </FooterTab>
                </Footer>
            </Container>
            </StyleProvider>
        );
    }
}
