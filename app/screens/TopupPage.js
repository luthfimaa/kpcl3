import React, { Component } from 'react';
import {
    Body,
    Container, 
    Header, 
    Button, 
    Content, 
    Drawer,
    Left,
    Picker,
    Form, 
    Item, 
    Icon,
    Input, 
    Label, 
    Right,
    StyleProvider,
    Text, 
    Title,
} from 'native-base';
import {Alert, Stylesheet } from 'react-native';
import SideBar from '../components/SideBar';

import UserController from '../controllers/UserController';

import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

export default class TopupPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            user: props.navigation.state.params.user,
            username: '',
            amount: '',
        };
    }
    topup(){
        UserController.topupBalance(this.state.username, parseInt(this.state.amount))
            .then(() => {
                Alert.alert('Pengisian saldo berhasil');
                this.props.navigation.navigate('Cashier', {user: this.state.user});
            })
    }
    openDrawer(){
        this._drawer._root.open();
    }
    render(){
        return(
    <StyleProvider style={getTheme(commonColor)}>
            <Drawer
            ref={ref => {this._drawer = ref}}
            content={
                <SideBar
                    user={this.state.user}
                    navigate={this.props.navigation.navigate}
                />
            }
            >
            <Container>
                <Header 
                androidStatusBarColor={variables.statusBar}
                style={{backgroundColor:'white'}}>
                <Left style={{flex: 1}}>
                    <Button transparent
                    onPress={this.openDrawer.bind(this)}
                    >
                        <Icon 
                        style={{color: variables.icon}}
                        name='menu'/>
                    </Button>
                </Left>
                <Body style={{flex: 1, justifyContent: 'center', aligntItems: 'center'}}>
                    <Title
                    style={{color: 'black'}}
                    >
                        Topup
                    </Title>
                </Body>

                <Right style={{flex: 1}}>
                </Right>
                </Header>
                <Content padder
                style={styles.content}
                >
                <Form>
                    <Item>
                        <Icon name='people'/>
                        <Input 
                        placeholder='Username'
                        value={this.state.username} 
                        onChangeText={text => this.setState({username: text})} />
                    </Item>
                    <Item>
                        <Icon name='logo-usd'/>
                        <Input 
                        placeholder='Jumlah Saldo'
                        value={this.state.amount} 
                        onChangeText={text => this.setState({amount: text})}
                        keyboardType="numeric"
                        />
                    </Item>
                    <Button block rounded
                    style={{marginTop: 30}}
                    onPress={this.topup.bind(this)}
                    >
                        <Text>
                            Topup
                        </Text>
                    </Button>
                </Form>

                </Content>
            </Container>
            </Drawer>
        </StyleProvider>
        );
    }
}
