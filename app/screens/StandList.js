import React, { Component } from 'react';
import {
    Container, 
    Header, 
    Button, 
    Body,
    Content, 
    Card,
    Carditem,
    Drawer,
    Icon,
    Left,
    List,
    ListItem,
    Right,
    StyleProvider,
    Spinner,
    Text, 
    Title,
} from 'native-base';

import {
    Alert,
    AsyncStorage,
    RefreshControl,
    Stylesheet,
} from 'react-native';

import {userData} from '../data/data';

import StandController from '../controllers/StandController';
import UserController from '../controllers/UserController';
import SideBar from '../components/SideBar';

import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

export default class StandList extends Component{
    constructor(props){
        super(props);
        let user = userData;
        if(props.navigation.state.params != undefined){
            user = props.navigation.state.params.user;
        }
        this.state = {
            ds: [],
            refreshing: true,
            user: user,
            navigate: props.navigation.navigate,
        };
    }

    componentWillMount(){
        this.getItems();
        this.getPersistentUser()
            .then(() => {
                this.getCurrentUser();
            });
    }

    getCurrentUser(){
        if(this.state.user.role != 'guest'){
            UserController.getCurrentUser()
                .then(user => {
                    this.setState({user: user});
                    try{
                        AsyncStorage.setItem('@User:key', JSON.stringify(user.toJSON()));
                    }catch(error) {
                        Alert.alert(error);
                    }
            });
        }
    }

    getItems(){
        StandController.getStandList()
            .then(standList => {
                this.setState({
                    ds: standList,
                    refreshing: false,
                });
            });
    }

    getPersistentUser(){
        return AsyncStorage.getItem('@User:key')
            .then(userStr => {
                if(userStr !== null) this.setState({user: JSON.parse(userStr)});
            });
    }

    renderRow(item){
        return(
            <ListItem
            button={true}
            onPress={() => {
                if(this.state.user.role != 'cashier'){
                    this.state.navigate("MenuList", {
                        stand: item,
                        user: this.state.user
                    })
                }
                else{
                    this.state.navigate("Transaction", {
                        stand: item,
                        user: this.state.user
                    })
                }
            }}>
                <Text>
                    {item.displayName}
                </Text>
            </ListItem>
        );
    
    }

    openDrawer(){
        this._drawer._root.open();
    }
    render(){
        return(
            <StyleProvider style={getTheme(commonColor)}>
                <Drawer
                ref={ref => {this._drawer = ref}}
                content={
                    <SideBar
                        user={this.state.user}
                        navigate={this.props.navigation.navigate}
                    />
                }
                >
                    <Container>
                        <Header 
                        androidStatusBarColor={variables.statusBar}
                        style={{backgroundColor:'white'}}>
                        <Left style={{flex: 1}}>
                            <Button transparent
                            onPress={this.openDrawer.bind(this)}
                            >
                                <Icon 
                                style={{color: variables.icon}}
                                name='menu'/>
                            </Button>
                        </Left>
                        <Body style={{flex: 1, justifyContent: 'center', aligntItems: 'center'}}>
                            <Title
                            style={{color: 'black'}}
                            >
                                Stand
                            </Title>
                        </Body>
                        <Right style={{flex: 1}}>
                        {this.state.user.role == 'member' &&
                            <Button transparent
                            onPress={() => this.state.navigate('CartPage', {
                                user: this.state.user,
                            })}
                            >
                                <Icon 
                                style={{color: variables.icon}}
                                name='ios-cart' />
                            </Button>
                        }
                        </Right>
                    </Header>
                        <Content
                        style={{borderTop: 1}}
                        refreshControl={<RefreshControl 
                        refreshing={this.state.refreshing}
                        onRefresh={() => {
                            this.setState({
                                refreshing: true,
                            });
                            this.getItems();
                        }}/> }
                        style={styles.content}>
                            <List dataArray={this.state.ds}
                            renderRow={this.renderRow.bind(this)}>
                            </List>
                        </Content>
                    </Container>
                </Drawer>
            </StyleProvider>
        );
    }
}
