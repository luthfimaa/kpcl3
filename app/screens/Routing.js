import React, { Component } from 'react';
import {Container, Header, Content, Button, Form, Item, Input, Label, Text, Right} from 'native-base';
import {Alert, AsyncStorage} from 'react-native';
import {userData} from '../data/data';
export default class Routing extends Component{

    constructor(props){
        super(props);
        this.state = {
            user: userData,
            navigate: props.navigation.navigate,
        };
    
    }

    componentWillMount(){
        this.getPersistentUser()
            .then(() => {
                let destination = 'StandList';
                    switch(this.state.user.role){
                        case 'member': destination = 'StandList'; break;
                        case 'admin': destination = 'StandList'; break;
                        case 'seller': destination = 'OrderPage'; break;
                        case 'cashier': destination = 'Cashier'; break;
                    }
                    this.state.navigate(destination, {user: this.state.user});
                })
            .catch(error => this.state.navigate('StandList', {user: this.state.user})); //if user not logged in
    }

    getPersistentUser(){
        return AsyncStorage.getItem('@User:key')
            .then(userStr => {
                if(userStr !== null) this.setState({user: JSON.parse(userStr)});
            });
    }

    render(){
        return(
            <Container>
            </Container>
        );
    }
}
