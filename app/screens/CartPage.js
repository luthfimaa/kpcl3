import React, { Component } from 'react';
import {
    Container, 
    Header, 
    Button, 
    Body,
    Content, 
    Card,
    CardItem,
    Col,
    Form,
    Footer,
    FooterTab,
    Grid,
    Input,
    Item,
    Label,
    List,
    ListItem,
    Right,
    StyleProvider,
    Text, 
    Title,
} from 'native-base';
import {
    Alert,
    StyleSheet,
    RefreshControl
} from 'react-native';

import CartController from '../controllers/CartController';
import CartItem from '../components/CartItem';

import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';
export default class CartPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            ds: [],
            navigate: props.navigation.navigate,
            user: props.navigation.state.params.user,
            tableNumber: '',
            isEmpty: false,
            totalPrice: 0,
            refreshing: true,
        };
    }
    getItems(){
        this.setState({
            refreshing: true,
            ds: [],
        });

        CartController.getOrderList(this.state.user.cartId)
            .then(orderList => {
                this.setState({
                    ds: orderList,
                    refreshing: false,
                });

                if(orderList.length == 0){
                    this.setState({isEmpty: true});
                }
            });
    }

    componentWillMount(){
        this.getItems();
        this.getPrice();
    }

    getPrice(){
        CartController.getTotalPrice(this.state.user.cartId)
            .then(price => this.setState({totalPrice: price.toString()}));
    }
    renderRow(item){
        return(
            <CartItem
            item={item}
            user={this.state.user}
            getPrice={this.getPrice.bind(this)}
            remove={this.removeFromList.bind(this)}
            />
        );

    }
    removeFromList(item){
        let array = this.state.ds;
        for(var i = 0; i < array.length; i++){
            if(array[i].id == item.id){
                array.splice(i, 1);
            }
        }
        this.setState({
            ds: []
        });
        this.setState({
            ds: array
        });
    }
    render(){
        return(
    <StyleProvider style={getTheme(commonColor)}>
            <Container>
                <Header 
                androidStatusBarColor={variables.statusBar}
                style={{backgroundColor:'white'}}>
                    <Body style={{flex: 1, justifyContent: 'center', aligntItems: 'center'}}>
                        <Title
                        style={{color: 'black'}}
                        >
                            Keranjang
                        </Title>
                    </Body>
                </Header>
                    <Content
                    refreshControl={<RefreshControl 
                    refreshing={this.state.refreshing}
                    onRefresh={() => {
                        this.getItems();
                    }}/> }

                    style={styles.content}
                    >
                        <List 
                        dataArray={this.state.ds}
                        renderRow={this.renderRow.bind(this)}
                        />
                    </Content>
                <Footer>
                    <FooterTab style={{ backgroundColor: 'white' }}
                    >
                    <Input
                    keyboardType='numeric'
                    placeholder='Nomor Meja'
                    onChangeText={text => {
                        this.setState({
                            tableNumber: text
                        });
                        if(text != ''){
                            CartController.setTableNumber(this.state.user.cartId, parseInt(text));
                        }
                        if(parseInt(text) > 49){
                            Alert.alert('Nomor Meja Tidak Lebih dari 50');
                            this.setState({tableNumber: ''});
                        }
                    }}
                    value={this.state.tableNumber}
                    />
                    </FooterTab>
                    <FooterTab>
                    <Button
                    style={styles.primaryButton}
                    onPress={() => {
                        if(this.state.totalPrice > this.state.user.balance){
                            Alert.alert('Saldo Anda Tidak Mencukupi');
                        }
                        else if(this.state.isEmpty){
                            Alert.alert('Keranjang Masih Kosong.');
                        }
                        else if(this.state.tableNumber == ''){
                            Alert.alert('Silahkan isikan nomor meja anda');
                        }
                        else{
                            CartController.purchase(this.state.user)
                                .then(() => {
                                    Alert.alert('Pemesanan Sukses. Silahkan Menunggu');
                                    this.state.navigate('StandList', {user: this.state.user});
                                })
                                .catch(error => Alert.alert(error.toString()));
                        }
                    }}
                    >
                        <Text
                        style={{color: 'white'}}
                        >Order (RP. {this.state.totalPrice})</Text>
                    </Button>
                    </FooterTab>
                </Footer>
            </Container>
        </StyleProvider>
        );
    }
}
