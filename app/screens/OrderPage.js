import React, { Component } from 'react';
import {
    Container, 
    Header, 
    Button, 
    Body,
    Content, 
    Col,
    Drawer,
    Form,
    Footer,
    FooterTab,
    Grid,
    Icon,
    Input,
    Item,
    Label,
    Left,
    List,
    ListItem,
    Right,
    Row,
    StyleProvider,
    Text, 
    Title,
} from 'native-base';
import {
    Alert,
    RefreshControl,
    StyleSheet } from 'react-native';

import StandController from '../controllers/StandController';
import OrderController from '../controllers/OrderController';
import SideBar from '../components/SideBar';
import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

export default class OrderPage extends Component{
    constructor(props){
        super(props);
        //let user = props.navigation.state.params.user
        this.state = {
            pendingDs: [],
            acceptedDs: [],
            user: props.navigation.state.params.user,
            stand: '',
            refreshing: false,
        };
    }

    componentWillMount(){
        StandController.getUserStand(this.state.user.id)
            .then(stand => {
                this.setState({stand: stand});
                this.getItems();
            })
    }

    getItems(){
        OrderController.getStandOrder(this.state.stand.id)
            .then(orderList => this.setState({
                pendingDs: orderList.filter(order => (order.status == 'pending')),
                acceptedDs: orderList.filter(order => (order.status == 'accepted'))
            }));
    }

    accept(item){
        OrderController.acceptOrder(item);
        this.getItems();
    }

    refuse(item){
        OrderController.refuseOrder(item);
        this.getItems();
    }

    renderRowPending(item){
        return(
            <ListItem>
                <Grid style={{flex: 1}}>
                    <Col size={5}>
                        <Text>
                            {item.name}
                        </Text>
                        <Text>
                            Jumlah : {item.quantity}
                        </Text>
                        <Text>
                            Nomor Meja : {item.tableNumber}
                        </Text>
                    </Col>
                    <Col size={2}>

                    </Col>
                    <Col size={4}>
                        <Row style={{marginBottom: 10}}>
                            <Button rounded bordered dark iconRight
                            onPress={this.accept.bind(this, item)}
                            >
                                <Text>
                                    Terima
                                </Text>
                                <Icon name='ios-checkmark'/>
                            </Button>
                        </Row>
                        <Row>
                            <Button rounded bordered iconRight
                            onPress={this.refuse.bind(this, item)}
                            >
                                <Text>
                                    Tolak
                                </Text>
                                <Icon name='ios-close'/>
                            </Button>

                        </Row>
                    </Col>
                </Grid>
            </ListItem>
        );
    }
    renderRowAccepted(item){
        return(
            <ListItem>
                <Grid>
                    <Col size={50}>
                        <Text>
                            {item.name}
                        </Text>
                        <Text>
                            Jumlah : {item.quantity}
                        </Text>
                        <Text>
                            Nomor Meja : {item.tableNumber}
                        </Text>
                    </Col>
                </Grid>
            </ListItem>
        );
    }
    openDrawer(){
        this._drawer._root.open();
    }

    render(){
        return(
    <StyleProvider style={getTheme(commonColor)}>
        <Drawer
        ref={ref => {this._drawer = ref}}
        content={
            <SideBar user={this.state.user} navigate={this.props.navigation.navigate}/>
        }>
            <Container>
                <Header 
                androidStatusBarColor={variables.statusBar}
                style={{backgroundColor:'white'}}>
                    <Left style={{flex: 1}}>
                        <Button transparent
                        onPress={this.openDrawer.bind(this)}
                        >
                            <Icon 
                            style={{color: variables.icon}}
                            name='menu'/>
                        </Button>
                    </Left>
                    <Body style={{flex: 1, justifyContent: 'center', aligntItems: 'center'}}>
                        <Title
                        style={{color: 'black'}}
                        >
                            Pesanan
                        </Title>
                    </Body>
                    <Right style={{flex:1}}>
                    </Right>
                </Header>
                <Content
                style={styles.content}
                refreshControl={<RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this.setState({refreshing: true})}
                />}
                >

                <List>
                    <ListItem header first>
                        <Text>PENDING</Text>
                    </ListItem>
                </List>
                <List
                dataArray={this.state.pendingDs}
                renderRow={this.renderRowPending.bind(this)}
                /> 
                <List>
                    <ListItem header first>
                        <Text>DITERIMA</Text>
                    </ListItem>
                </List>
                <List
                dataArray={this.state.acceptedDs}
                renderRow={this.renderRowAccepted.bind(this)}
                /> 
                </Content>

            </Container>
        </Drawer>
    </StyleProvider>
        );
    }
}
