import React, { Component } from 'react';
import {
    Button,
    Body,
    Container,
    Content,
    Form, 
    Header,
    Icon,
    Item, 
    Input, 
    Label, 
    Right,
    StyleProvider,
    Text, 
    Title,
} from 'native-base';
import {
    Alert,
    RefreshControl
} from 'react-native';
import AuthController from '../controllers/AuthController';
import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

export default class Register extends Component{
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            name: '',
            username: '',
            phone: '',
            loading: false,
        };
    }
    onPressRegister(){
        this.setState({
            loading: true,
        });
        AuthController.register(this.state.email, this.state.password, this.state.username, this.state.name, this.state.phone)
        .then(() => {
            this.setState({
                loading: false,
            });
            Alert.alert('Registrasi Berhasil. Silahkan Login');
            this.props.navigation.navigate('Login');
        })
        .catch(error => {
            this.setState({loading: false});
            Alert.alert(error);
        })
            
    }
    render(){
        return(
            <StyleProvider style={getTheme(commonColor)}>
            <Container>
                <Header 
                androidStatusBarColor={variables.statusBar} 
                style={{backgroundColor:'white'}}>
                <Body>
                    <Title style={{color: 'black'}}>
                        Register
                    </Title>
                </Body>
                </Header>
                <Content padder
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.loading}
                    />
                }
                style={styles.content}
                >
                    <Form>
                        <Item>
                            <Icon name='ios-mail'/>
                            <Input
                            value={this.state.email}
                            placeholder="Email"
                            onChangeText={text => this.setState({email: text})}
                            />
                        </Item>
                        <Item>
                            <Icon name='ios-lock'/>
                            <Input
                            placeholder="Password"
                            value={this.state.password}
                            secureTextEntry={true}
                            onChangeText={text => this.setState({password: text})}

                            />
                        </Item>
                        <Item>
                            <Icon name='ios-person'/>
                            <Input
                            placeholder="Nama Lengkap"
                            value={this.state.name}
                            onChangeText={text => this.setState({name: text})}
                            />
                        </Item>
                        <Item>
                            <Icon name='ios-people'/>
                            <Input
                            placeholder="Username"
                            value={this.state.username}
                            onChangeText={text => this.setState({username: text})}
                            />
                        </Item>
                        <Item >
                            <Icon name='ios-phone-portrait'/>
                            <Input
                            placeholder="Nomor HP"
                            value={this.state.phone}
                            onChangeText={text => this.setState({phone: text})}
                            />
                        </Item>
                        <Button block rounded primary
                        onPress={this.onPressRegister.bind(this)}
                        >
                            <Text>
                                Register
                            </Text>
                        </Button>
                    </Form>
                </Content>
            </Container>
            </StyleProvider>
        );
    }
}

