import React, { Component } from 'react';
import {
    Button, 
    Container, 
    Col,
    Content, 
    Grid,
    Header, 
    Row,
    StyleProvider,
    Text, 
} from 'native-base';
import { Alert } from 'react-native';
import AuthController from '../controllers/AuthController';

import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

export default class Cashier extends Component{
    constructor(props){
        super(props);
        this.state = {
            user: props.navigation.state.params.user,
            navigate: props.navigation.navigate,
        };
    }
    render(){
        return(
            <StyleProvider style={getTheme(commonColor)}>
                <Container>
                    <Header androidStatusBarColor={variables.statusBar} 
                    style={{display:'none'}}/>
                    <Grid style={{backgroundColor: 'white'}}>
                        <Row size={7}>

                        </Row>
                        <Row>
                        <Col>
                            <Button block rounded
                            onPress={() => this.state.navigate('TopupPage', {user: this.state.user})}
                            >
                                <Text>
                                    Topup Saldo
                                </Text>
                            </Button>
                        </Col>

                        </Row>
                        <Row>
                            <Col>
                                <Button block rounded
                                onPress={() => this.state.navigate('StandList', {user: this.state.user})}
                                >
                                    <Text>
                                        Transaksi Penjual
                                    </Text>
                                </Button>
                            </Col>
                        </Row>
                    </Grid>
                </Container>
            </StyleProvider>
        );
    }
}
