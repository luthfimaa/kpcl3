import React, { Component } from 'react';
import {
    Button,
    Body,
    Container,
    Content,
    Form,
    Footer,
    FooterTab,
    Header,
    Icon,
    Item,
    Input,
    Label,
    Left,
    Right,
    Spinner,
    StyleProvider,
    Text,
    Title,
} from 'native-base';

import { 
    Alert,
    AsyncStorage,
    RefreshControl,
} from 'react-native';

import AuthController from '../controllers/AuthController';

import {styles, variables} from '../styles/styles';
import getTheme from '../../native-base-theme/components';
import commonColor from '../../native-base-theme/variables/commonColor';

export default class Login extends Component{
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            loading: false,
        };
    }

    login(){
        this.setState({loading: true});
        AuthController.login(this.state.email, this.state.password)
            .then(user => {
                this.setState({loading: false});
                let screen = 'StandList';
                if(user.role == 'cashier') screen = 'Cashier';
                if(user.role == 'seller') screen = 'OrderPage';
                this.props.navigation.navigate(screen, {user: user});
            }).catch(error => {
                this.setState({loading: false});
                Alert.alert(error.toString());
            });
    }

    render(){
        return(
            <StyleProvider style={getTheme(commonColor)}>
                <Container>
                    <Header 
                    androidStatusBarColor={variables.statusBar} 
                    style={{backgroundColor:'white'}}>
                    <Body>
                        <Title style={{color: 'black'}}>
                            Login
                        </Title>
                    </Body>
                    </Header>
                        <Content
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.loading}
                            />
                        }
                        style={styles.content} padder>
                            <Form>
                                <Item>
                                    <Icon name='ios-person'/>
                                    <Input
                                    value={this.state.email}
                                    placeholder='Email'
                                    onChangeText={text => this.setState({email: text})}
                                    />
                                </Item>
                                <Item>
                                <Icon name='lock'/>
                                <Input 
                                placeholder='Password'
                                value={this.state.password}
                                secureTextEntry={true}
                                onChangeText={text => this.setState({password: text})}
                                />
                                </Item>

                                <Button block rounded 
                                style={{marginTop: 50}}
                                onPress={this.login.bind(this)}
                                >
                                    <Text style={{color: 'white'}}>
                                    Login
                                    </Text>
                                </Button>

                            </Form>
                        </Content>
                </Container>
            </StyleProvider>
        );
    }
}
