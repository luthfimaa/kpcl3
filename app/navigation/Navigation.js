import { AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';

import Welcome from '../screens/Welcome';
import Login from '../screens/Login';
import Register from '../screens/Register';
import StandList from '../screens/StandList';
import MenuList from '../screens/MenuList';
import MenuForm from '../screens/MenuForm';
import CartPage from '../screens/CartPage';
import OrderPage from '../screens/OrderPage';
import TopupPage from '../screens/TopupPage';
import Transaction from '../screens/Transaction';
import Cashier from '../screens/Cashier';
import Routing from '../screens/Routing';


const noHeader = {
    header: null
}

export const Navigation = StackNavigator({
    Routing: {
        screen: Routing,
        navigationOptions: noHeader,
    },
    StandList: {
        screen: StandList,
        navigationOptions: noHeader,
    },
    Login: {
        screen: Login,
        navigationOptions: noHeader,
    },
    Cashier: {
        screen: Cashier,
        navigationOptions: noHeader,
    },
    TopupPage: {
        screen: TopupPage,
        navigationOptions: noHeader,
    },
    OrderPage: {
        screen: OrderPage,
        navigationOptions: noHeader,
    },
    CartPage: {
        screen: CartPage,
        navigationOptions: noHeader,
    },
    Welcome: {
        screen: Welcome,
        navigationOptions: noHeader,
    },
    MenuList: {
        screen: MenuList,
        navigationOptions: noHeader,
    },
    MenuForm: {
        screen: MenuForm,
        navigationOptions: noHeader,
    },
    Register: {
        screen: Register,
        navigationOptions: noHeader,
    },
    Transaction: {
        screen: Transaction,
        navigationOptions: noHeader,
    },
});

AppRegistry.registerComponent('Navigation', () => Navigation);
