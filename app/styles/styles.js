import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    content: {
        backgroundColor: 'white',
    },
    list: {
        minHeight: 100,
    },
    card: {
        padding: 10,
    },
    text: {
        fontSize: 16,
    },
    menuCard: {
        margin: 0,
        flex: 1,
        elevation: 0,
        borderWidth: 1,
        borderColor: 'red',
        elevation: 0,
    },
    head: {
        height: 40,
        backgroundColor: '#f1f8ff'
    },
    text: {
        marginLeft: 5
    },
    row: {
        height: 30 
    },
    padButton: {
        padding: 10,
    },
    primaryButton: {
        backgroundColor: '#F6362F',
        color: 'white',
    },
    inverseButton: {
        backgroundColor: 'white',
        color: '#F6362F',
    },
});

const colors = {
    black: '#000000',
    brandPrimary: "#F6362F",
    brandInfo: "#62B1F6",
    brandSuccess: "#5cb85c",
    brandDanger: "#d9534f",
    brandWarning: "#f0ad4e",
    brandSidebar: "#252932",
    brandDark: "#000",
    brandLight: "#f4f4f4",
};
const variables = {
    statusBar: colors.black,
    icon: colors.brandPrimary,
};

export{
    styles,
    variables,
    colors,
};
